
package demo;

public class Calculator {
	private static int result; // 静态变量，用于存储运行结果
	public void add(int n) {
		result = result + n;
	}
	public void setInitialValue(int i) {
		result = i;
	}
	public void substract(int n) {
		result = result - n; // Bug: 正确的应该是 result =result-n
	}
	public void multiply(int n) {
		result = result * n;
	} // 此方法尚未写好
	public void divide(int n) throws ArithmeticException {
    	if (n != 0) {
    		result = result / n;
    	} else {
    		throw new ArithmeticException("除0异常");
    	}
}

	public void square(int n) {
		result = n * n;
	}
	public void squareRoot(int n) {
		Math.sqrt(n);
	//	for (;;)
		//	; // Bug : 死循环
	}
	public void clear() { // 将结果清零
		result = 0;
	}
	public int getResult() {
		return result;
	}
}
