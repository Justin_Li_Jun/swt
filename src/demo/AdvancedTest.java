package demo;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class AdvancedTest {

	private static Calculator calculator = new Calculator();
	private int param;
	private int result;

	// 构造方法，对变量进行初始化
	public AdvancedTest(int param, int result) {
		this.param = param;
		this.result = result;
	}
	
	@Parameters
	public static Collection data() {
		return Arrays.asList(new Object[][]{
				{2,4},
				{0,0},
				{-3,9}
		});
	}
	
	@Before
	public void clearCalculator() throws Exception {
		calculator.clear();
	}

	@Test
	public void testSquare() {
		calculator.square(param);
		assertEquals(result, calculator.getResult());
	}
}

