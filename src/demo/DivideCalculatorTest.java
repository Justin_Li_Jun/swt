package demo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DivideCalculatorTest {
	private static Calculator calculator = new Calculator();

	@Before
	public void setUp() throws Exception {
	}

	@Test(expected = ArithmeticException.class)
	public void testDivideByZero() {
		calculator.setInitialValue(20);
		calculator.divide(0);
		
	}


}
