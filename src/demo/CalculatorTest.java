package demo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class CalculatorTest {
	private static Calculator calculator = new Calculator();

	@Before
	public void setUp() throws Exception {
		calculator.clear();
	}

	@Test
	public void testAdd() {
		calculator.setInitialValue(3);
		calculator.add(1);
		assertEquals(4,calculator.getResult());
		
	}

	@Test
	public void testSubstract() {
		calculator.setInitialValue(4);
		calculator.substract(2);
		assertEquals(2,calculator.getResult());
		
	}

	@Test
	public void testMultiply() {
		calculator.setInitialValue(6);
		calculator.multiply(2);
		assertEquals(12,calculator.getResult());
		
	}


	@Test(expected = ArithmeticException.class)
	public void testDivideByZero() {
		calculator.divide(0);
	}

	@Ignore("Multiply() Not yet implemented")
	@Test
	public void testSquare() {
		calculator.setInitialValue(80);
		calculator.square(4);
		assertEquals(16,calculator.getResult());
		
	}

	@Test
	public void testSquareRoot() {
		calculator.setInitialValue(9);
		calculator.squareRoot(9);
		assertEquals(3,calculator.getResult());
		
	}

}
